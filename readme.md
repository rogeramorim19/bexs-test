## Teste Banco Bexs

* Este é o projeto feito para resolver o problema proposto pelo banco bexs. Nesta aplicação podemos criar rotas de destino com seus respectivos valores, podemos listar todas as rotas criadas, e verificar a disponibilidade de uma rota com seu melhor custo. Nele busquei abstrair o máximo de uso de frameworks externos e demais libs, assim focando na lógica, construção arquitetural e design da aplicação.

### Tecnologias:

* Java 11 (JDK)
* Gradle
* Spring MVC
* Spring Web Tools

### Testes unitários:

* Junit
* Mockito

### Design Patterns:

* Fiz somento o uso do design factory, para abstração dos métodos da classe de serviço.

### Modelo Arquitetural:

* Por conta da simplicidade desse projeto utilizei o modelo MVC (Model, View, Controller) de maneira simplificada sem o uso da camada de model, já que não existe conexão com banco de dados, uso apenas dados em memória para melhor escalar o response time da aplicação.

### Como executar a aplicação?

* Para facilitar a execução dessa aplicação, criei um shell script que esta na raiz do projeto, ao ser executado ele irá realizar o build da aplicação já rodando os testes unitários e fará o deploy da aplicação na porta 8085.
* Basta ir até a raiz do projeto e executar o seguinte comando: ($ bash build.sh).
* A aplicação estará disponível no seguinte endereço: http://localhost:8085/bexs.
* Observação: Atentar-se ao uso de demais processos na porta 8085, pois se houver algum processo sendo executado a aplicação não irá realizar o deploy com sucesso. Sugiro executar o seguinte comando para deixar livre o PID ($ kill 8085).

### Como testar a aplicação:

* A aplicação dispoe de três Api's, sendo elas:
     * POST http://localhost:8085/bexs (Criação de novas rotas de destino com seus respectivos preços).
     * GET http://localhost:8085/bexs/all (Listagem de todas as rotas disponíveis).
     * GET http://localhost:8085/bexs?outboundRoute=GRU&inboundRoute=MCZ (Escolha da melhor rota de destino com seu melhor custo, basta entrar com os parâmetros de inbount e outbound).
     
* Para facilitar os testes, criei um arquivo na raiz do projeto chamado (collection.txt) nele contém o link para importar a collection com as requests via postman.

## Considerações Finais:

* Busquei resolver o problema proposto de maneira mais relevante e simples possível, focando no design na aplicação e no modelo arquitetural. Faço o uso de dados em memória, assim deixando as Apis com mais desempenho. Espero que gostem :)    