package com.bexs.constants;

public class BexsConstants {

    public static final String BEXS_PATH = "/bexs";
    public static final String FIND_ALL_ROUTES_PATH = "/all";
    public static final String INBOUND_ROUTE_PARAM = "inboundRoute";
    public static final String OUTBOUND_ROUTE_PARAM = "outboundRoute";
    public static final String MESSAGE_REQUIRED_FIELD = "This field is required";
    public static final String MESSAGE_SUCCESS_CREATE_ROUTE = "Creating a new destination route successfully!";
    public static final String MESSAGE_NOT_FOUND_ROUTE = "We couldn't find any route to the desired destination :(";
}
