package com.bexs.dto;

import com.bexs.constants.BexsConstants;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class BexsDtoV1 {

    @NotBlank
    @NotNull(message = BexsConstants.MESSAGE_REQUIRED_FIELD)
    @Builder.Default
    private final String inboundRoute = null;

    @NotBlank
    @NotNull(message = BexsConstants.MESSAGE_REQUIRED_FIELD)
    @Builder.Default
    private final String outboundRoute = null;

    @Builder.Default
    private final BigDecimal price = BigDecimal.ZERO;
}
