package com.bexs.api;

import com.bexs.constants.BexsConstants;
import com.bexs.dto.BexsDtoV1;
import com.bexs.service.BexsService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Objects;
import java.util.stream.Stream;

@AllArgsConstructor
@RequestMapping(BexsConstants.BEXS_PATH)
@RestController
public class BexsController {

    @Autowired private final BexsService bexsService;

    @PostMapping
    public ResponseEntity<?> createRoute(@Valid @RequestBody final BexsDtoV1 bexsDtoV1){
        bexsService.persistRouteMemory(bexsDtoV1);
        return new ResponseEntity<>(BexsConstants.MESSAGE_SUCCESS_CREATE_ROUTE, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getBetterRoute(@RequestParam(value = BexsConstants.INBOUND_ROUTE_PARAM) final String inboundRoute,
                                            @RequestParam(value = BexsConstants.OUTBOUND_ROUTE_PARAM) final String outboundRoute) {

        final var betterRouteSelected = bexsService.findBetterRoute(BexsDtoV1.builder()
                .outboundRoute(outboundRoute)
                .inboundRoute(inboundRoute)
                .build());

        if(Objects.isNull(betterRouteSelected)) {
            return new ResponseEntity<>(BexsConstants.MESSAGE_NOT_FOUND_ROUTE, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(betterRouteSelected, HttpStatus.OK);

    }

    @GetMapping(BexsConstants.FIND_ALL_ROUTES_PATH)
    public ResponseEntity<Stream<BexsDtoV1>> getAllRoutes() {
        return new ResponseEntity<>(bexsService.findAllRoutes(), HttpStatus.OK);
    }
}
