package com.bexs.service;

import com.bexs.dto.BexsDtoV1;

import java.util.stream.Stream;

public interface BexsService {

    void persistRouteMemory(final BexsDtoV1 bexsDtoV1);
    BexsDtoV1 findBetterRoute(final BexsDtoV1 bexsDtoV1);
    Stream<BexsDtoV1> findAllRoutes();
}
