package com.bexs.service;

import com.bexs.dto.BexsDtoV1;
import com.bexs.util.StreamUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Slf4j
@Service
public class BexsServiceImpl implements BexsService {

    final Map<Integer, BexsDtoV1> routeMap = new HashMap<>();

    @Override
    public void persistRouteMemory(final BexsDtoV1 bexsDtoV1) {
        routeMap.put(routeMap.size() + 1, bexsDtoV1);
    }

    @Override
    public BexsDtoV1 findBetterRoute(final BexsDtoV1 bexsDtoV1) {
        List<BexsDtoV1> bexsDtoList = new ArrayList<>();

        routeMap.forEach( (index, bexsDto) -> {
            if(bexsDto.getOutboundRoute().equals(bexsDtoV1.getOutboundRoute()) &&
                bexsDto.getInboundRoute().equals(bexsDtoV1.getInboundRoute())) {
                bexsDtoList.add(bexsDto);
            }
        });

        if(bexsDtoList.isEmpty()) {
            log.info("Destination route unavailable inbound [{}] outbound [{}]", bexsDtoV1.getInboundRoute(), bexsDtoV1.getOutboundRoute());
            return null;
        }

        return bexsDtoList.stream()
                .filter(Objects::nonNull)
                .min(Comparator.comparing(BexsDtoV1::getPrice))
                .orElse(null);
    }

    @Override
    public Stream<BexsDtoV1> findAllRoutes() {
        return routeMap.values().stream()
                .filter(StreamUtils.distinctByKey(BexsDtoV1::getPrice));
    }
}