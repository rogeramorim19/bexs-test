package com.bexs.service;

import com.bexs.dto.BexsDtoV1;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RunWith(MockitoJUnitRunner.class)
public class BexsServiceTest {

    @Mock
    private BexsService bexsService;

    private final Map<Integer, BexsDtoV1> routeMap = new HashMap<>();

    @Before
    public void setup() {
        final BexsDtoV1 oneRoute = new BexsDtoV1("GRU", "MCZ", new BigDecimal(250));
        final BexsDtoV1 twoRoute = new BexsDtoV1("BRC", "SCL", new BigDecimal(400));
        final BexsDtoV1 threeRoute = new BexsDtoV1("ORL", "CDG", new BigDecimal(350));
        final BexsDtoV1 fourRoute = new BexsDtoV1("GRU", "MCZ", new BigDecimal(300));

        routeMap.put(1, oneRoute);
        routeMap.put(2, twoRoute);
        routeMap.put(3, threeRoute);
        routeMap.put(4, fourRoute);
    }

    @Test
    public void persistRouteMemory() {
        final BexsDtoV1 findOneRoute = routeMap.values().stream()
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);

        bexsService.persistRouteMemory(findOneRoute);
        Assert.assertNotNull(findOneRoute);
    }

    @Test
    public void findBetterRoute() {
        final BexsDtoV1 choiceRoute = new BexsDtoV1("GRU", "MCZ", new BigDecimal(250));
        final BexsDtoV1 betterRouteSelected = routeMap.values().stream()
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);

        Assert.assertNotNull(betterRouteSelected);
        Assert.assertEquals(betterRouteSelected, choiceRoute);
    }

    @Test
    public void findAllRoutes() {
        final Map<Integer, BexsDtoV1> newRouteMap = new HashMap<>();
        final BexsDtoV1 oneRoute = new BexsDtoV1("GRU", "MCZ", new BigDecimal(250));
        final BexsDtoV1 twoRoute = new BexsDtoV1("BRC", "SCL", new BigDecimal(400));
        final BexsDtoV1 threeRoute = new BexsDtoV1("ORL", "CDG", new BigDecimal(350));
        final BexsDtoV1 fourRoute = new BexsDtoV1("GRU", "MCZ", new BigDecimal(300));

        newRouteMap.put(1, oneRoute);
        newRouteMap.put(2, twoRoute);
        newRouteMap.put(3, threeRoute);
        newRouteMap.put(4, fourRoute);

        Assert.assertNotNull(routeMap);
        Assert.assertEquals(newRouteMap, routeMap);
    }
}